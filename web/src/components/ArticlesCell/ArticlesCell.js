import Article from "../Article/Article"

export const QUERY = gql`
  query ArticlesQuery {
    articles: posts {
      id
      title
      body
      createdAt
    }
  }
`

export const Loading = () => <div className="p-5">Loading...</div>

export const Empty = () => <div className="p-5">Empty</div>

export const Failure = ({ error }) => (
  <div className="p-5 text-red-400">Error: {error.message}</div>
)

export const Success = ({ articles }) => {
  return (
    <ul
      className="first:m-0"
    >
      {articles.map((article) => {
        return <Article key={article.id} article={article} truncated={true} />
      })}
    </ul>
  )
}
