import Article from "../Article/Article"

export const QUERY = gql`
  query FindArticleQuery($id: Int!) {
    article: post(id: $id) {
      id
      title
      body
      createdAt
    }
  }
`

export const Loading = () => <div className="p-5">Loading...</div>

export const Empty = () => <div className="p-5">Empty</div>

export const Failure = ({ error }) => (
  <div className="p-5 text-red-400">Error: {error.message}</div>
)

export const Success = ({ article }) => {
  return <Article article={article} />
}
