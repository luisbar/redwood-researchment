// Define your own mock data here:
export const standard = () => ({
  article: {
    id: 42,
    title: 'title 1',
    body: 'Street art hexagon messenger bag gatekeep artisan vinyl PBR&B biodiesel viral squid. Retro austin green juice, taxidermy chartreuse kitsch chia taiyaki you probably haven not heard of them vexillologist tumblr pabst selfies. La croix tilde yuccie edison bulb cred. Readymade paleo williamsburg, JOMO bushwick typewriter distillery gatekeep fingerstache truffaut chillwave vibecession. Vinyl bespoke meditation vibecession master cleanse migas 1.',
    createdAt: 'Thu Sep 01 2022 13:32:27 GMT-0300 (-03)',
  },
})
