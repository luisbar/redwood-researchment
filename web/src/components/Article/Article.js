import { Link, routes } from '@redwoodjs/router'
import CommentsCell from 'src/components/CommentsCell'
import CommentForm from 'src/components/CommentForm'

const Article = ({ article, truncated = false }) => {
  return (
    <article
      className="p-5"
    >
      <header>
        <h2>
          <Link
            to={routes.article({ id: article.id })}
            className="text-accent-100 text-2xl"
          >
            {article.title}
          </Link>
        </h2>
      </header>
      <p
        className={`text-justify ${truncated ? 'truncate' : ''}`}
      >
        {article.body}
      </p>
      <div
        className="text-xs mt-5"
      >
        Posted at: {article.createdAt}
      </div>
      {!truncated && (
        <div className="mt-12">
          <CommentForm postId={article.id} />
          <div className="mt-12">
            <CommentsCell postId={article.id} />
          </div>
        </div>
      )}
    </article>
  )
}

export default Article
