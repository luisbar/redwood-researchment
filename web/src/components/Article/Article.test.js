import { render, waitFor } from '@redwoodjs/testing/web'
import { standard } from 'src/components/CommentsCell/CommentsCell.mock'

import Article from './Article'

const ARTICLE = {
  id: 1,
  title: 'First post',
  body: `Neutra tacos hot chicken prism raw denim, put a bird on it enamel pin post-ironic vape cred DIY. Street art next level umami squid. Hammock hexagon glossier 8-bit banjo. Neutra la croix mixtape echo park four loko semiotics kitsch forage chambray. Semiotics salvia selfies jianbing hella shaman. Letterpress helvetica vaporware cronut, shaman butcher YOLO poke fixie hoodie gentrify woke heirloom.`,
  createdAt: new Date().toISOString(),
}

describe('Article', () => {
  it('renders successfully', () => {
    expect(() => {
      render(<Article article={{ id: 1, title: 'title', body: 'body' }} />)
    }).not.toThrow()
  })

  it('renders comments when displaying a full blog post', async () => {
    const comment = standard().comments[0]
    const { getByText } =  render(<Article article={ARTICLE} />)

    await waitFor(() =>
      expect(getByText(comment.body)).toBeInTheDocument()
    )
  })

  it('does not render comments when displaying a summary', async () => {
    const comment = standard().comments[0]
    const { queryByText } = render(<Article article={ARTICLE} summary={true} />)

    await waitFor(() =>
      expect(queryByText(comment.body)).not.toBeInTheDocument()
    )
  })
})
