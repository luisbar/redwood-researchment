import { Link, routes } from '@redwoodjs/router'
import { useAuth } from '@redwoodjs/auth'
import { Toaster } from '@redwoodjs/web/toast'

const BlogLayout = ({ children }) => {
  const { isAuthenticated, currentUser, logOut } = useAuth()

  return (
    <>
      <Toaster />
      <header
        className="bg-dark-100 p-5 grid grid-rows-2 grid-cols-2 shadow-2xl"
      >
        <div
          className="flex-between row-start-1 col-start-1"
        >
          <h1>
            <Link
              className="text-primary-dark text-6xl"
              to={routes.home()}>Redwood Blog
            </Link>
          </h1>
        </div>
        <nav
          className="row-start-2 col-start-1"
        >
          <ul
            className="flex flex-row"
          >
            <li>
              <Link className="mr-5" to={routes.home()}>Home</Link>
            </li>
            <li>
              <Link className="mr-5" to={routes.about()}>About</Link>
            </li>
            <li>
              <Link to={routes.contact()}>Contact</Link>
            </li>
          </ul>
        </nav>
        {isAuthenticated ? (
          <div
            className="row-start-1 col-start-2 flex flex-row items-center justify-end"
          >
            <span>Logged in as {currentUser.email}</span>{' '}
            <button
              className="bg-accent-100 self-center justify-self-end p-2 rounded-md ml-5"
              type="button"
              onClick={logOut}
            >
              Logout
            </button>
          </div>
        ) : (
          <Link
            className="row-start-1 col-start-2 bg-accent-100 self-center justify-self-end p-2 rounded-md"
            to={routes.login()}
          >
              Login
          </Link>
        )}
      </header>
      <main
        className="bg-dark-100 h-full"
      >
        {children
      }</main>
    </>
  )
}

export default BlogLayout