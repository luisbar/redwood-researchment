/** @type {import('tailwindcss').Config} */
const colors = require('tailwindcss/colors');

module.exports = {
  content: ['src/**/*.{js,jsx,ts,tsx}'],
  theme: {
    colors: {
      ...colors,
      'primary': '#818CF8',
      'primary-light': '#A5B4FB',
      'primary-dark': '#6466f1',
      'accent-100': '#F7B1AB',
      'accent-200': '#81E4DA',
      'accent-300': '#47E5BC',
      'light-100': '#FFFFFF',
      'dark-100': '#343434',
      'dark-200': '#3F3F46',
      'dark-300': '#CACACA',
      'dark-400': '#FAFAFA',
      'error': '#F8A5A5',
      'success': '#86EFAC',
    },
    extend: {},
  },
  plugins: [
    require('@tailwindcss/forms'),
  ],
}
